'use strict';

eventsApp.controller('EventController', function ($scope, eventData, $log) {
    $scope.snippet        = '<span style="color:red"> hi there</span>';
    $scope.boolValue      = true;
    $scope.buttonDisabled = true;
    $scope.sortOrder      = 'name';
    $scope.event          = eventData.getEvent();   
    $scope.upVoteSession  = function (session) {
        session.upVoteCount++;
    };
    $scope.downVoteSession = function (session) {
        session.upVoteCount--;
    };
});
